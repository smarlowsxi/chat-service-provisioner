# SPDX-FileCopyrightText: 2023 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

default:
  users: &defaultUsers
    - "@sebastien.heurtematte": 100
    - "@ef_sync_bot": 100
    - "@ef_moderator_bot": 60
  defaultProjectSpace: &defaultProjectSpace
    - "#eclipse-projects"
  userPowerLevel: 50
  roomPowerLevel:
    ban: 55
    events:
      m.reaction: 0
      m.room.avatar: 55
      m.room.canonical_alias: 55
      m.room.encryption: 100
      m.room.history_visibility: 100
      m.room.name: 55
      m.room.pinned_events: 50
      m.room.power_levels: 100
      m.room.redaction: 0
      m.room.server_acl: 60
      m.room.tombstone: 100
      m.room.topic: 50
      m.space.child: 55
      org.matrix.msc3401.call: 50
      org.matrix.msc3401.call.member: 50
    events_default: 0
    historical: 100
    invite: 50
    kick: 55
    redact: 55
    state_default: 50
    users_default: 0
  roomVisibility: "public"
  roomPreset: "public_chat"

projects:
  - eclipsefdn:
      rooms:
        - alias: "#eclipsefdn"
          name: "Eclipse Foundation"
          type: space
          forceUpdate: true
          topic: |
            Open communication channel at Eclipse Foundation
        - alias: "#eclipsefdn.it"
          name: "IT"
          parent: "#eclipsefdn"
          forceUpdate: true
          topic: |
            Welcome to the IT team room! This room is a space for the Eclipse Foundation IT staff to discuss, report, and troubleshoot technical issues related to our organization's infrastructure and systems. 
            And don't forget to report issues via [Eclipse HelpDesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new)
        - alias: "#eclipsefdn.chat-support"
          name: "chat service support"
          parent: "#eclipsefdn"
          forceUpdate: true
          topic: |
            Welcome to the chat support room! This room is to provide help and assistance with our chat service. Feel free to ask questions, report issues, or share feedback. Full FAQ is available [here](https://chat.eclipse.org/docs/faq/)
        - alias: "#eclipsefdn.general"
          name: "general"
          parent: "#eclipsefdn"
          forceUpdate: true
          topic: |
            Welcome to the general room! This room is a place for users to chat and discuss a wide range of topics about the chat service, the foundation, Eclipse projects, ECA, contribution, ...
            Feel free to introduce yourself, share your interests, and start ask questions. Full Documentation is available [here](https://chat.eclipse.org/docs/)
            Want new rooms/spaces, please follow intructions [here](https://chat.eclipse.org/docs/faq/#request-for-roomspace)
        - alias: "#eclipsefdn.ip-license-compliance"
          name: "IP License compliance"
          parent: "#eclipsefdn"
          forceUpdate: true
          topic: |
            Welcome to the IP license compliance room!

  - eclipse-projects:
      rooms:
        - alias: "#eclipse-projects"
          name: "Eclipse Projects"
          type: space
          forceUpdate: true
          topic: |
            Welcome to the Eclipse projects space! This space is dedicated to discussions and collaboration around projects developed under the Eclipse Foundation.
            We encourage active participation and constructive discussions. Please feel free to share your ideas, ask for advice, provide feedback, report issues... This space is a community hub for Eclipse-related projects, and we welcome all perspectives and contributions.
            To ensure a respectful environment, please be aware of our [privacy policy](https://www.eclipse.org/legal/privacy.php) and follow our [code of conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php) and `guidelines`. 
            Full documentation can be found [here](https://chat.eclipse.org/docs/) and [FAQ](https://chat.eclipse.org/docs/faq)

  - eclipse-newcomers:
      rooms:
        - alias: "#eclipse-newcomers"
          name: "Eclipse Newcomers"
          parent:
            - *defaultProjectSpace
          topic: |
            Welcome to the Eclipse Newcomers Room! Feel free to ask questions, seek guidance, and connect with other newcomers and experienced community members.

  - adoptium:
      rooms:
        - alias: "#adoptium"
          type: space
          name: "Eclipse Adoptium™"
          topic: |
            The mission of the Eclipse Adoptium Top-Level Project is to produce high-quality runtimes and associated technology for use within the Java ecosystem.  We achieve this through a set of Projects under the Adoptium PMC and a close working partnership with external projects, most notably OpenJDK for providing the Java SE runtime implementation.  Our goal is to meet the needs of both the Eclipse community and broader runtime users by providing a comprehensive set of technologies around runtimes for Java applications that operate alongside existing standards, infrastructures, and cloud platforms.
            The AdoptOpenJDK project was established in 2017 following years of discussions about the general lack of an open and reproducible build and test system for OpenJDK source across multiple platforms.  Since then it has grown to become a leading provider of high-quality OpenJDK-based binaries used by enterprises in embedded systems, desktops, traditional servers, modern cloud platforms, and large mainframes.  The Eclipse Adoptium project is the continuation of the original AdoptOpenJDK mission.
        - alias: "#adoptium-general"
          name: "Adoptium General"
          parent:
            - *defaultProjectSpace
            - "#adoptium"
          topic: |
            https://github.com/adoptium/adoptium - starting point for build farm folks
            https://drive.google.com/drive/folders/1jjdXw3fAgRIeupb_bXCoOgFH9_iyo1ak - meeting agendas / minutes
        - alias: "#adoptium-build"
          name: "Adoptium Build"
          parent:
            - *defaultProjectSpace
            - "#adoptium"
          topic: |
            Discussions related to the building of anything in the Adoptium machine farm
            Nightly triage: https://docs.google.com/document/d/1vcZgHJeR8rW8U8OD23Uob7A1dbLrtkURZUkinUp7f_w/edit?usp=sharing
        - alias: "#adoptium-infrastructure"
          name: "Adoptium Infrastructure"
          parent:
            - *defaultProjectSpace
            - "#adoptium"
          topic: |
            Discussion relating to the machines and services supporting OpenJDK build & test. Machine list @ https://github.com/adoptium/infrastructure/blob/master/ansible/inventory.yml
        - alias: "#adoptium-testing-aqavit"
          name: "Adoptium Testing Aqavit"
          parent:
            - *defaultProjectSpace
            - "#adoptium"
          topic: |
            Topics relating to testing activities done as part of the activities under the AQAvit project
        - alias: "#adoptium-containers"
          name: "Adoptium Containers"
          parent:
            - *defaultProjectSpace
            - "#adoptium"
          topic: |
            Topics relating to containers
        - alias: "#adoptium-installer"
          name: "Adoptium Installer"
          parent:
            - *defaultProjectSpace
            - "#adoptium"
          topic: |
            Topics relating to installer
        - alias: "#adoptium-release"
          name: "Adoptium Release"
          parent:
            - *defaultProjectSpace
            - "#adoptium"
          topic: |
            Topics relating to release: https://github.com/adoptium/temurin-build/blob/master/RELEASING.md
        - alias: "#adoptium-secure-dev"
          name: "Adoptium Secure Dev"
          parent:
            - *defaultProjectSpace
            - "#adoptium"
          topic: |
            Topics relating to Secure Dev: https://docs.google.com/document/d/1bxYCEbM4Wn2uLl_lgY67a6x5VBWH_ZGdLwG_yDByfT0/edit?pli=1#heading=h.bti4iq4qm4f0

  - automotive:
      rooms:
        - alias: "#automotive"
          name: "Eclipse Automotive"
          parent: *defaultProjectSpace
          topic: |
            The Eclipse Automotive Top-Level Project provides a space for open source projects to explore ideas and technologies addressing challenges in the automotive, mobility and transportation domain. It is the common goal of this project to provide tools and composable building blocks to empower the development of solutions for the mobility of the future.
  - automotive.sumo:
      rooms:
        - alias: "#automotive.sumo"
          name: "Eclipse SUMO™"
          topic: "General discussions about the Eclipse SUMO (Simulation of Urban MObility) project"
          parent: *defaultProjectSpace
  - automotive.tractusx:
      rooms:
        - alias: "#automotive.tractusx"
          type: space
          name: "Tractus-X"
          topic: |
            Eclipse Tractus-X™ space.
        - alias: "#tractusx"
          extraAlias: "#tools.tractus-x"          
          name: "Tractus-X"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            Eclipse Tractus-X™ aims at the development of systems in support of the Catena-X data space, with focus on PLM / quality, resiliency, sustainability, and shared network services.
        - alias: "#tractusx-ssi"
          extraAlias: "#tools.tractus-x-ssi"
          name: "SSI"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            Eclipse Tractus-X™ component SSI
        - alias: "#tractusx-bpdm"
          extraAlias: "#tools.tractusx-bpdm"
          name: "BPDM"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            Business Partner Data Management, BPDM Components, Value Added Services.
        - alias: "#tractusx-country-risk"
          extraAlias: "#tools.tractusx-country-risk"
          name: "Country Risk"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            Room for Country Risk topics discussion and alignment.
        - alias: "#tractusx-committers"
          name: "Committers"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            A room for all the committers from the Tractus-X Project. The topics discussed in this room will be defined in the [Open hour meeting](https://eclipse-tractusx.github.io/community/open-meetings) for Eclipse Tractus-X committers.
        - alias: "#tractusx-edc"
          extraAlias: "#tools.tractusx-edc"
          name: "EDC"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            The Tractus-X EDC provides pre-built control- and data-plane docker images and helm charts of the Eclipse DataSpaceConnector Project.
        - alias: "#tractusx-edc-architecture"
          extraAlias: "#tools.tractusx-edc-architecture"
          name: "EDC Architecture"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            Alignment about Tractus-X EDC roadmap, business requirements, architecture and PRs.
        - alias: "#tractusx-irs"
          name: "Item Relationship Service"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            The vision for the Item Relationship Service is to provide an easy access endpoint for complex distributed digital twins across Catena-X members. It abstracts the access from separated digital twins towards a connected data chain of twins and provides those. It enables to apply business logic on complex distributed digital twins across company borders.
        - alias: "#tractusx-portal"
          name: "Portal"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            The Portal and Marketplace is the entry point for all activities in the automotive dataspace. This room focuses on Portal and Marketplace related topics as well as on Identity and Access Management.
        - alias: "#tractusx-security"
          name: "Security"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            Room for security topics, discussions and alignment.
        - alias: "#tractusx-trace-x"
          name: "Trace-X"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            Trace-X is a system for tracking parts along the supply chain.
        - alias: "#tractusx-umbrella-chart"
          name: "Umbrella chart"
          parent:
            - *defaultProjectSpace
            - "#automotive.tractusx"
          topic: |
            Room for discussion and collaboration for the overarching Umbrella chart.
  - automotive.velocitas:
      rooms:
        - alias: "#velocitas"
          name: "Eclipse Velocitas"
          parent: *defaultProjectSpace
          topic: |
            Eclipse Velocitas provides an end-to-end, scalable, modular and open source development toolchain for creating containerized and non-containerized in-vehicle applications.
  - dt.esmf:
      rooms:
        - alias: "#eclipse-semantic-modeling-framework"
          extraAlias: "#eclipse-esmf"
          name: "Eclipse Semantic Modeling Framework"
          parent: *defaultProjectSpace
          topic: |
            Discussions and support queries for the Eclipse Semantic Modeling Framework and its Semantic Aspect Meta Model, and working with Aspect Models (with or without Aspect Model Editor).
            See also https://github.com/eclipse-esmf, https://projects.eclipse.org/projects/dt.esmf
  - ecd.theia:
      rooms:
        - alias: "#ecd.theia"
          name: "Eclipse Theia"
          parent: *defaultProjectSpace
          topic: |
            Eclipse Theia™ is an extensible platform to develop full-fledged, multi-language, cloud & desktop IDE-like products with state-of-the-art web technologies.
  - eclipse.ide:
      rooms:
        - alias: "#eclipse-ide"
          type: space
          name: "Eclipse IDE"
          topic: |
            This is the space of the Eclipse IDE Foundation's flagship "IDE" Community, it includes all Eclipse open source projects that participate in the Eclipse IDE simultaneous release.
        - alias: "#eclipse-ide-newcomers"
          name: "Eclipse-IDE Newcomers"
          parent:
            - *defaultProjectSpace
            - "#eclipse-ide"
          topic: |
            Eclipse IDE Newcomers Room: feel free to ask questions, seek guidance, and connect with other newcomers and experienced community members.
        - alias: "#eclipse-ide-general"
          name: "Eclipse-IDE General"
          parent:
            - *defaultProjectSpace
            - "#eclipse-ide"
          topic: |
            General Community discussions on IDE and all Eclipse open source projects that participate in the Eclipse IDE simultaneous release
        - alias: "#eclipse-ide-steeringcommittee"
          name: "Eclipse-IDE Steering Committee"
          parent:
            - *defaultProjectSpace
            - "#eclipse-ide"
          topic: |
            Eclipse IDE Working Group Steering Committee discussions.
  - ee4j.krazo:
      rooms:
        - alias: "#ee4j.krazo"
          name: "Eclipse Krazo™"
          parent: *defaultProjectSpace
          topic: |
            Eclipse Krazo™ is an implementation of action-based MVC specified by MVC 1.0 (JSR-371). It builds on top of JAX-RS and currently contains support for RESTEasy, Jersey and CXF with a well-defined SPI for other implementations.
  - ee4j.mvc:
      rooms:
        - alias: "#ee4j.mvc"
          name: "Jakarta MVC™"
          parent: *defaultProjectSpace
          topic: |
            Jakarta Model-View-Controller, or Jakarta MVC™ for short, is a common pattern in Web frameworks where it is used predominantly to build HTML applications. The model refers to the application’s data, the view to the application’s data presentation and the controller to the part of the system responsible for managing input, updating models and producing output. Web UI frameworks can be categorized as action-based or component-based. In an action-based framework, HTTP requests are routed to controllers where they are turned into actions by application code; in a component-based framework, HTTP requests are grouped and typically handled by framework components with little or no interaction from application code. In other words, in a component-based framework, the majority of the controller logic is provided by the framework instead of the application.
  - ee4j.rest:
      rooms:
        - alias: "#jaxrs"
          type: space
          name: "Jakarta RESTful Web Services™"
          topic: |
            Jakarta RESTful Web Services™ provides a specification document, TCK and foundational API to develop web services following the Representational State Transfer (REST) architectural pattern. JAX-RS: Java API for RESTful Web Services (JAX-RS) is a Java programming language API spec that provides support in creating web services according to the Representational State Transfer (REST) architectural pattern.
        - alias: "#ee4j.rest"
          name: "Jakarta RESTful Web Services™"
          parent:
            - *defaultProjectSpace
            - "#jaxrs"
          topic: |
            Jakarta RESTful Web Services™ provides a specification document, TCK and foundational API to develop web services following the Representational State Transfer (REST) architectural pattern. JAX-RS: Java API for RESTful Web Services (JAX-RS) is a Java programming language API spec that provides support in creating web services according to the Representational State Transfer (REST) architectural pattern.
  - iot.4diac:
      rooms:
        - alias: "#eclipse-4diac"
          type: space
          name: "Eclipse 4diac™"
          topic: |
            Eclipse 4diac™ provides an open source infrastructure for distributed industrial process measurement and control systems based on the IEC 61499 standard.
        - alias: "#4diac-townsquare"
          name: "Eclipse 4diac Town Square"
          parent:
            - *defaultProjectSpace
            - "#eclipse-4diac"
          topic: |
            A town square for the Eclipse 4diac project, the place for conversations that do not fit any dedicated Matrix room.
        - alias: "#4diac-ide"
          name: "Eclipse 4diac IDE"
          parent:
            - *defaultProjectSpace
            - "#eclipse-4diac"
          topic: |
            Questions and discussions targeting 4diac IDE.
        - alias: "#4diac-forte"
          name: "Eclipse 4diac FORTE"
          parent:
            - *defaultProjectSpace
            - "#eclipse-4diac"
          topic: |
            Questions and discussions targeting 4diac FORTE.
  - iot.kiso-testing:
      rooms:
        - alias: "#kiso-testing"
          type: space
          name: "kiso testing"
          topic: |
            Space to discuss pykiso topics
        - alias: "#kiso-testing-committer"
          name: "kiso testing committer"
          parent:
            - *defaultProjectSpace
            - "#kiso-testing"
          topic: |
            Strategy discussions regarding pykiso (roadmap, next features, next PRs to merge)
        - alias: "#kiso-testing-contributor"
          name: "kiso testing contributor"
          parent:
            - *defaultProjectSpace
            - "#kiso-testing"
          topic: |
            Technical discussions regarding pykiso and the different contributions
  - jakartaee-wg:
      rooms:
        - alias: "#jakartaee-wg"
          type: space
          name: "Jakarta EE Working Group Space"
          topic: |
            Space to discuss Jakarta EE Working topics
        - alias: "#jakartaee-wg-participant-members"
          name: "Jakarta EE Working Group Participant Members"
          parent:
            - *defaultProjectSpace
            - "#jakartaee-wg"
          topic: |
            Channel for participant members interested to be involved and having on-going conversations
  - modeling:
      rooms:
        - alias: "#eclipse-modeling-build"
          type: space
          name: "Eclipse Modeling builds"
          topic: |
            Space related to all modeling project build rooms
  - modeling.emf.mwe:
      rooms:
        - alias: "#modeling-emf-mwe-builds"
          name: "Eclipse Modeling Workflow Engine Builds"
          parent:
            - *defaultProjectSpace
            - "#eclipse-modeling-build"
          topic: |
            Eclipse Modeling Workflow Engine Builds
  - modeling.gemoc:
      rooms:
        - alias: "#gemoc-studio"
          name: "Eclipse GEMOC Studio"
          parent: *defaultProjectSpace
          topic: |
            The Eclipse GEMOC Studio is a language workbench for designing and integrating EMF-based modeling languages. It gives a special focus on behavior integration by providing a framework with a generic interface to plug in different execution engines!
  - modeling.tmf.xtext:
      rooms:
        - alias: "#eclipse-xtext"
          type: space
          name: "Eclipse Xtext"
          topic: |
            Eclipse Xtext™ is a framework for development of programming languages and domain specific languages. It covers all aspects of a complete language infrastructure, from parsers, over linker, compiler or interpreter to fully-blown top-notch Eclipse IDE integration. It comes with good defaults for all these aspects and at the same time every single aspect can be tailored to your needs.
        - alias: "#xtext"
          name: "Eclipse Xtext"
          parent:
            - *defaultProjectSpace
            - "#eclipse-xtext"
          topic: |
            Eclipse Xtext
        - alias: "#xtext-releng"
          name: "Xtext Builds"
          parent:
            - *defaultProjectSpace
            - "#eclipse-xtext"
            - "#eclipse-modeling-build"
          topic: |
            Notification Channel for Xtext Build Results
  - modeling.xpect:
      rooms:
        - alias: "#modeling-xpect-build"
          name: "Eclipse Xpect Builds"
          parent:
            - *defaultProjectSpace
            - "#eclipse-modeling-build"
          topic: |
            Notification Channel for Eclipse Xpect™ Builds
  - oniro:
      rooms:
        - alias: "#oniro"
          type: space
          name: "Oniro"
          topic: |
            The mission of the Eclipse Oniro is the design, development, production and maintenance of an open source software platform, having an operating system, an ADK/SDK, standard APIs and basic applications, like UI, as core elements, targeting different industries thanks to a next-generation multi-kernel architecture, that simplifies the existing landscape of complex systems, and its deployment across a wide range of devices.
        - alias: "#oniro-project"
          name: "Oniro Project"
          parent:
            - *defaultProjectSpace
            - "#oniro"
          projectId: "oniro.oniro-core"
          topic: |
            A town square for Oniro Project, the place for conversations that do not fit any dedicated Matrix room.
        - alias: "#oniro4openharmony"
          name: "Oniro for OpenHarmony"
          parent:
            - *defaultProjectSpace
            - "#oniro"
          projectId: "oniro.oniro4openharmony"
          topic: |
            Eclipse Oniro for OpenHarmony
        - alias: "#oniro-ide"
          name: "Oniro IDE"
          parent:
            - *defaultProjectSpace
            - "#oniro"
          topic: |
            Development of IDE for Oniro.
        - alias: "#oniro-chromium"
          name: "Oniro Chromium"
          parent:
            - *defaultProjectSpace
            - "#oniro"
          topic: |
            Development of Chromium-related projects for Oniro.
        - alias: "#oniro-marketing"
          name: "Oniro Marketing"
          parent:
            - *defaultProjectSpace
            - "#oniro"
          topic: |
            Oniro Marketing channel is the space to discuss and coordinate the execution of the activities defined by the WG Marketing committee.
  - science:
      rooms:
        - alias: "#science"
          type: space
          name: "Eclipse Science"
          topic: |
            The Science top-level project provides a central clearinghouse for collaborative development efforts to create software for scientific research and development.
        - alias: "#science-general"
          name: "Eclipse Science General"
          parent:
            - *defaultProjectSpace
            - "#science"
          topic: |
            General room for engaging on Eclipse Science Projects and Science Top-Level Project work.
        - alias: "#ice"
          extraAlias: "#science-ice"
          name: "Eclipse ICE"
          parent:
            - *defaultProjectSpace
            - "#science"
          projectId: "science.ice"
          topic: |
            The Eclipse Integrated Computational Environment (ICE) is a scientific workbench and workflow environment developed to improve the user experience for computational scientists.
  - technology.jgit:
      rooms:
        - alias: "#technology.jgit.reviews"
          name: "Eclipse JGit Reviews"
          parent: *defaultProjectSpace
          topic: |
            Discuss JGit changes
  - technology.lsp4j:
      rooms:
        - alias: "#eclipse-lsp4j-builds"
          extraAlias: "#technology.lsp4j-builds"
          name: "Eclipse LSP4J Builds"
          parent: *defaultProjectSpace
          topic: |
            Notification Channel for Eclipse LSP4J Build Results
  - technology.os-gov:
      rooms:
        - alias: "#os-gov"
          name: "OS-Gov"
          parent: *defaultProjectSpace
          topic: |
            Discussions for the Eclipse OS-Gov project
  - technology.packager:
      rooms:
        - alias: "#packager"
          name: "Eclipse Packager"
          parent: *defaultProjectSpace
          topic: |
            The Eclipse Packager™ project offers a set of core functionality to work with RPM and Debian package files in plain Java. This functionality is offered in simple JAR variant to create your own solutions, or in ready-to-run tools like an Apache Maven plugin.
  - technology.xpanse:
      rooms:
        - alias: "#eclipse-xpanse"
          type: space
          name: "xpanse"
          topic: |
            xpanse is here to make native cloud services configurable and portable.
        - alias: "#technology.xpanse"
          name: "Eclipse Xpanse"
          parent:
            - *defaultProjectSpace
            - "#eclipse-xpanse"
          topic: |
            Place for all conversations related to eclipse-xpanse.
  - tools.mylyn:
      rooms:
        - alias: "#tools.mylyn"
          name: "Eclipse Mylyn"
          parent: *defaultProjectSpace
          topic: |
            Eclipse Mylyn is a Task-Focused Interface for Eclipse that reduces information overload and makes multi-tasking easy. The mission of the Mylyn project is to provide: 1. Frameworks and APIs for Eclipse-based task and Application Lifecycle Management (ALM) 2. Exemplary tools for task-focused programming within the Eclipse IDE. 3. Reference implementations for open source ALM tools used by the Eclipse community and for open ALM standards such as OSLC The project is structured into sub-projects, each representing an ALM category and providing common APIs for specific ALM tools. The primary consumers of this project are ALM ISVs and other adopters of Eclipse ALM frameworks.  Please see the project charter for more details. Mylyn makes tasks a first class part of Eclipse, and integrates rich and offline editing for repositories such as Bugzilla, Trac, and JIRA. Once your tasks are integrated, Mylyn monitors your work activity to identify information relevant to the task-at-hand, and uses this task context to focus the Eclipse UI on the interesting information, hide the uninteresting, and automatically find what's related. This puts the information you need to get work done at your fingertips and improves productivity by reducing searching, scrolling, and navigation. By making task context explicit Mylyn also facilitates multitasking, planning, reusing past efforts, and sharing expertise.
